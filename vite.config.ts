import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import babel from '@rollup/plugin-babel'

// https://vitejs.dev/config/
export default defineConfig({
  input: 'src/main.tsx',
  output: {
    dir: 'output',
    format: 'esm'
  },
  plugins: [react(), babel({ babelHelpers: 'bundled' })]
})
