# To-do app

Magroove internship program challenge.

Demo: https://magroove.gitlab.io/

## Development

- [Git](https://git-scm.com/)
- [Yarn](https://yarnpkg.com/)
- [Vite](https://vitejs.dev/)
- [Babel](https://babeljs.io/)
- [Tailwind CSS](https://tailwindcss.com/)

## Installation

Clone the repository inside your projects folder:

```
git clone https://gitlab.com/magroove/to-do.git
cd to-do
```

Install the dependencies:

```
yarn install
```

Run the app in dev mode:

```
yarn dev
```
