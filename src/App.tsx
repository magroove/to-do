import React from "react";
import { useState } from "react";
import "./App.css";
import Button from "./components/Button";
import TextInput from "./components/TextInput";
import TodoItem from "./components/TodoItem";

interface ITodoProps {}

interface ITodoState {
  to_do_list: any;
  value: string;
}
class Todo extends React.Component<ITodoProps, ITodoState> {
  constructor(props: ITodoProps) {
    super(props);

    this.switchIsDone = this.switchIsDone.bind(this);

    this.handleChange = this.handleChange.bind(this);

    this.state = {
      value: "",
      to_do_list: [
        { description: "Finish this test", is_done: false },
        { description: "Get hired", is_done: false },
        { description: "Change the world", is_done: false },
      ],
    };

    this.addItem = this.addItem.bind(this);
  }
  switchIsDone(index: any) {
    const to_do_list = this.state.to_do_list;

    to_do_list[index].is_done = !to_do_list[index].is_done;

    this.setState({
      to_do_list: to_do_list,
    });
  }
  handleChange(e: any) {
    this.setState({
      value: e.target.value,
    });
  }
  addItem() {
    if (this.state.value.length > 0) {
      this.setState({
        value: "",
        to_do_list: [
          ...this.state.to_do_list,
          { description: this.state.value, is_done: false },
        ],
      });
    }
  }
  render() {
    return (
      <div>
        <div className="m-auto mt-10 max-w-sm rounded overflow-hidden p-10 shadow-lg">
          <h2 className="text-xl my-5">Add a new task to your to-do list</h2>
          <TextInput
            placeholder="E.g: Do math worksheet"
            value={this.state.value}
            onChange={this.handleChange}
          />
          <Button onClick={this.addItem}>Add</Button>
          <ul>
            {this.state.to_do_list.map((task: any) => (
              <TodoItem
                is_done={task.is_done}
                onClick={() => {
                  this.switchIsDone(this.state.to_do_list.indexOf(task));
                }}
                key={this.state.to_do_list.indexOf(task)}
              >
                {task.description}
              </TodoItem>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

function App() {
  return <Todo />;
}

export default App;
