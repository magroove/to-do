class ToDoList extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        to_do_list: [
          { description: "Finish this test", is_done: false },
          { description: "Get hired", is_done: false },
          { description: "Change the world", is_done: false },
        ],
        item: "",
      };
      this.addItem = this.addItem.bind(this);
      this.handleChange = this.handleChange.bind(this);
    }
  
    handleChange(e) {
      this.setState({
        item: e.target.value,
      });
    }
  
    addItem() {
      if (this.state.item.length > 0)
        this.setState({
          to_do_list: [
            ...this.state.to_do_list,
            { description: this.state.item, is_done: false },
          ],
        });
    }
  
    render() {
      return (
        <div>
          <h2>Add a new task to your to-do list</h2>
         
          <button onClick={this.addItem}>Add</button>
          <ul>
            {this.state.to_do_list.map((task) => (
              <ToDoItem task={task} key={task.description} />
            ))}
          </ul>
        </div>
      );
    }
}