import React, { FC } from "react";

interface IButtonState {}
interface IButtonProps {
  onClick: any;
}

class Button extends React.Component<IButtonProps, IButtonState> {
  constructor(props: IButtonProps) {
    super(props);
  }
  render() {
    return (
      <button
        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-2 text-lg my-2  rounded float-right w-full"
        onClick={this.props.onClick}
      >
        {this.props.children}
      </button>
    );
  }
}

export default Button;
