import React, { FC } from "react";

interface IToDoItemProps {
  is_done: boolean
  onClick: any;
}

class ToDoItem extends React.Component<IToDoItemProps> {
  constructor(props: IToDoItemProps) {
    super(props);
  }
  render() {
    return (
      <div
        className={`cursor-pointer hbg-gray-50 hover:bg-gray-200 text-grey-100 py-2 px-2 text-lg rounded float-right w-full ${this.props.is_done ? "line-through" : ""}`}
        onClick={this.props.onClick}
      >
        {this.props.children}
      </div>
    );
  }
}

export default ToDoItem;
