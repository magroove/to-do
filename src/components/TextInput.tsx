import React, { FC } from "react";

// interface ITextInputState {
// }
interface ITextInputProps {
  value: string;
  placeholder: string;
  onChange: any;
}
class TextInput extends React.Component<ITextInputProps/*, ITextInputState*/> {
  constructor(props: ITextInputProps) {
    super(props);
  }
  render() {
    return (
      <input
        type="text"
        value={this.props.value}
        placeholder={this.props.placeholder}
        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        onChange={this.props.onChange}
      />
    );
  }
}

export default TextInput;
